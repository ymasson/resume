FROM debian:bullseye-slim
LABEL maintainer "Yann Masson"

ENV container docker
ENV DEBIAN_FRONTEND noninteractive

RUN \
    echo 'APT::Install-Recommends "0";' >/etc/apt/apt.conf.d/01norecommend && \
    echo 'APT::Install-Suggests "0";' >>/etc/apt/apt.conf.d/01norecommend && \
    echo 'deb http://deb.debian.org/debian bullseye-backports main' > /etc/apt/sources.list.d/backports.list && \
  apt-get update && \
  apt-get dist-upgrade -y && \
  apt-get install --no-install-recommends -y \
    openssl \
    wget \
    latexmk \
    texlive-fonts-extra \
    texlive-fonts-recommended \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-latex-recommended \
    texlive-luatex \
    texlive-xetex \
    texlive-pictures \
    texlive-lang-french \
    lmodern \
    fonts-font-awesome \
    && \
  # \
  apt-get autoremove -y && \
  apt-get autoclean -y && \
  apt-get clean -y && \
  rm -rf /usr/share/locale/* && \
  rm -rf /var/cache/debconf/* && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /usr/share/doc/* && \
  rm -rf /tmp/* /var/tmp/*

